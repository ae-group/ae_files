<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.95 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.14 -->
# files 0.3.24

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_files/develop?logo=python)](
    https://gitlab.com/ae-group/ae_files)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_files/release0.3.23?logo=python)](
    https://gitlab.com/ae-group/ae_files/-/tree/release0.3.23)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_files)](
    https://pypi.org/project/ae-files/#history)

>ae_files module 0.3.24.

[![Coverage](https://ae-group.gitlab.io/ae_files/coverage.svg)](
    https://ae-group.gitlab.io/ae_files/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_files/mypy.svg)](
    https://ae-group.gitlab.io/ae_files/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_files/pylint.svg)](
    https://ae-group.gitlab.io/ae_files/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_files)](
    https://gitlab.com/ae-group/ae_files/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_files)](
    https://gitlab.com/ae-group/ae_files/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_files)](
    https://gitlab.com/ae-group/ae_files/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_files)](
    https://pypi.org/project/ae-files/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_files)](
    https://gitlab.com/ae-group/ae_files/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_files)](
    https://libraries.io/pypi/ae-files)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_files)](
    https://pypi.org/project/ae-files/#files)


## installation


execute the following command to install the
ae.files module
in the currently active virtual environment:
 
```shell script
pip install ae-files
```

if you want to contribute to this portion then first fork
[the ae_files repository at GitLab](
https://gitlab.com/ae-group/ae_files "ae.files code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_files):

```shell script
pip install -e .[dev]
```

the last command will install this module portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_files/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.files.html
"ae_files documentation").
